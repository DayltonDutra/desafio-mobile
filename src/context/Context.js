import { createContext, useContext, useReducer } from "react";
// import faker from "faker";
import { faker } from "@faker-js/faker";
import { cartReducer, productReducer } from "./Reduces";

const Cart = createContext();
faker.seed(99);

function capitalizeWords(string) {
  return string.replace(/(\b\w)/g, function (match) {
    return match.toUpperCase();
  });
}

function generateBookImageURL() {
  const bookId = faker.number.int({ min: 20, max: 60 });
  return `https://picsum.photos/id/${bookId}/200/300`;
}

const Context = ({ children }) => {
  const products = [...Array(20)].map(() => ({
    id: faker.string.uuid(),
    name: capitalizeWords(faker.word.words()),
    price: faker.commerce.price({ min: 15, max: 100 }),
    image: generateBookImageURL(),
    inStock: faker.helpers.arrayElement([0, 3, 5, 6, 7]),
    fastDelivery: faker.datatype.boolean(),
    ratings: faker.helpers.arrayElement([3, 4, 5]),
    author: faker.person.fullName(),
  }));

  // console.log(products);
  const [state, dispatch] = useReducer(cartReducer, {
    products: products,
    cart: [],
  });

  const [productState, productDispatch] = useReducer(productReducer, {
    byStock: false,
    byFastDelivery: false,
    byRating: 0,
    searchQuery: "",
  });

  return (
    <Cart.Provider value={{ state, dispatch, productState, productDispatch }}>
      {children}
    </Cart.Provider>
  );
};

export default Context;

export const CartState = () => {
  return useContext(Cart);
};
