import React, { useState } from "react";
import {
  Badge,
  Button,
  Container,
  Dropdown,
  FormControl,
  Nav,
  Navbar,
} from "react-bootstrap";
import { FaShoppingCart } from "react-icons/fa";
import { Link } from "react-router-dom";
import { CartState } from "../context/Context";
import { AiFillDelete } from "react-icons/ai";

const Header = () => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const toggleDropdown = () => {
    setIsDropdownOpen(!isDropdownOpen);
  };

  const closeDropdown = () => {
    setIsDropdownOpen(false);
  };

  const {
    state: { cart },
    dispatch,
    productDispatch,
  } = CartState();

  return (
    <Navbar variant="dark" sticky="top" className="navBar">
      <Container>
        <Navbar.Brand>
          <Link to="/">Loja de Livros</Link>
        </Navbar.Brand>
        <Navbar.Text className="search">
          <FormControl
            style={{ width: 500 }}
            placeholder="Buscar Livro"
            className="m-auto"
            onChange={(e) => {
              productDispatch({
                type: "FILTER_BY_SEARCH",
                payload: e.target.value,
              });
            }}
          />
        </Navbar.Text>
        <Nav>
          <Dropdown onToggle={toggleDropdown} show={isDropdownOpen}>
            <Dropdown.Toggle className="cartIcon">
              <FaShoppingCart color="white" fontSize="25px" />
              <Badge pill className="counter" bg="info">
                {cart.length}
              </Badge>
            </Dropdown.Toggle>

            <Dropdown.Menu style={{ minWidth: 370 }}>
              {cart.length > 0 ? (
                <>
                  {cart.map((prod) => (
                    <span className="cartItem" key={prod.id}>
                      <img
                        src={prod.image}
                        className="cartItemImg"
                        alt={prod.name}
                      />

                      <div className="cartItemDetail">
                        <span>{prod.name}</span>
                        <span>R$ {prod.price.replace(".", ",")}</span>
                      </div>

                      <AiFillDelete
                        fontSize="20px"
                        style={{ cursor: "pointer" }}
                        onClick={() =>
                          dispatch({
                            type: "REMOVE_FROM_CART",
                            payload: prod,
                          })
                        }
                      />
                    </span>
                  ))}
                  <Link to="/cart" onClick={closeDropdown}>
                    <Button
                      className="toCartBtn"
                      style={{ width: "95%", margim: "0 10px" }}
                    >
                      Ir para o carrinho
                    </Button>
                  </Link>
                </>
              ) : (
                <span style={{ padding: 10 }}>O Carrinho está vazio!</span>
              )}
            </Dropdown.Menu>
          </Dropdown>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default Header;
