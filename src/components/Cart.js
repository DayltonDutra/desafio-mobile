import { Button, Col, Form, Image, ListGroup, Row } from "react-bootstrap";
import { CartState } from "../context/Context";
import { useEffect, useState } from "react";
import Rating from "./Rating";
import { AiFillDelete } from "react-icons/ai";

const Cart = () => {
  const {
    state: { cart },
    dispatch,
  } = CartState();

  const [totalWithoutDiscount, setTotalWithoutDiscount] = useState(0);
  const [total, setTotal] = useState();
  const [discountTotal, setDiscountTotal] = useState(0);

  useEffect(() => {
    setTotalWithoutDiscount(
      cart.reduce((acc, curr) => acc + Number(curr.price) * curr.qty, 0).toLocaleString("pt-BR", {
        style: "currency",
        currency: "BRL",
      })
    );

    const subTotal = cart.reduce(
      (acc, curr) => acc + Number(curr.price) * curr.qty,
      0
    );

    const distinctBooks = new Set(cart.map((prod) => prod.id));
    const distinctBooksCount = distinctBooks.size;

    let discount = 0;

    if (distinctBooksCount === 2) {
      discount = 0.05; 
    } else if (distinctBooksCount === 3) {
      discount = 0.1; 
    } else if (distinctBooksCount === 4) {
      discount = 0.2; 
    } else if (distinctBooksCount >= 5) {
      discount = 0.25; 
    }

    const totalDiscount = subTotal * discount;
    const totalWithDiscount = subTotal - totalDiscount;

    setTotal(totalWithDiscount.toLocaleString("pt-BR", {
      style: "currency",
      currency: "BRL",
    }));
    setDiscountTotal(totalDiscount.toLocaleString("pt-BR", {
      style: "currency",
      currency: "BRL",
    }));
  }, [cart]);

  return (
    <div className="home">
      <div className="productContainer">
        <ListGroup>
          {cart.map((prod) => (
            <ListGroup.Item key={prod.id}>
              <Row>
                <Col md={2}>
                  <Image src={prod.image} alt={prod.name} fluid rounded />
                </Col>
                <Col md={2}>
                  <span>{prod.name}</span>
                </Col>
                <Col md={2}>
                  <span>{prod.price}</span>
                </Col>
                <Col md={2}>
                  <Rating rating={prod.ratings} />
                </Col>
                <Col>
                  <Form.Control
                    as="select"
                    value={prod.qty}
                    onChange={(e) =>
                      dispatch({
                        type: "CHANGE_CART_QTY",
                        payload: {
                          id: prod.id,
                          qty: e.target.value,
                        },
                      })
                    }
                  >
                    {[...Array(prod.inStock).keys()].map((x) => (
                      <option key={x + 1}>{x + 1}</option>
                    ))}
                  </Form.Control>
                </Col>
                <Col md={2}>
                  <Button
                    type="button"
                    variant="light"
                    onClick={() =>
                      dispatch({
                        type: "REMOVE_FROM_CART",
                        payload: prod,
                      })
                    }
                  >
                    <AiFillDelete fontSize="20px" />
                  </Button>
                </Col>
              </Row>
            </ListGroup.Item>
          ))}
        </ListGroup>
      </div>
      <div className="filters summary">
        <span className="title">Subtotal ({cart.length}) itens</span>
        <span style={{ fontWeight: 500, fontSize: 20 }}>
          Subtotal: {totalWithoutDiscount}
        </span>
        <span style={{ fontWeight: 500, fontSize: 20 }}>
          Desontos: - {discountTotal}
        </span>
        <span style={{ fontWeight: 700, fontSize: 22 }}>
          Total a pagar: {total}
        </span>
        <Button
          type="button"
          className="CheckoutBtn"
          disabled={cart.length === 0}
        >
          Ir para o Checkout
        </Button>
      </div>
    </div>
  );
};

export default Cart;
