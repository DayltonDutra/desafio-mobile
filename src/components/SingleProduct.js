import { Button, Card } from "react-bootstrap";
import Rating from "./Rating";
import { CartState } from "../context/Context";
import { BsCartX } from "react-icons/bs";

const SingleProduct = ({ prod }) => {
  const {
    state: { cart },
    dispatch,
  } = CartState();

  return (
    <div className="products">
      <Card>
        <Card.Img variant="top" src={prod.image} alt={prod.name} />
        <Card.Body>
          <Card.Title style={{ fontSize: 16 }}>{prod.name}</Card.Title>
          <Card.Subtitle style={{ paddingBottom: 10 }}>
            <div style={{ fontSize: 12 }}>Autor: {prod.author}</div>
            <span>R$ {prod.price.replace(".", ",")}</span>
            {prod.fastDelivery ? (
              <div style={{ fontSize: 12 }}>Entrega no mesmo dia</div>
            ) : (
              <div style={{ fontSize: 12 }}>5 dias para entrega</div>
            )}
            <Rating rating={prod.ratings} />
          </Card.Subtitle>
          {cart.some((p) => p.id === prod.id) ? (
            <Button
              onClick={() => {
                dispatch({
                  type: "REMOVE_FROM_CART",
                  payload: prod,
                });
              }}
              // variant="secondary"
              className="removeCartBtn"
            >
              Remover 
              <BsCartX fontSize="20px" style={{marginBottom: "4px"}} />
            </Button>
          ) : (
            <Button
              onClick={() => {
                dispatch({
                  type: "ADD_TO_CART",
                  payload: prod,
                });
              }}
              disabled={!prod.inStock}
              className="addCartBtn"
            >
              {!prod.inStock ? "Sem estoque" : "Comprar"}
            </Button>
          )}
        </Card.Body>
      </Card>
    </div>
  );
};

export default SingleProduct;
